from unittest import TestCase

from bowling_game import BowlingGame


class test_bowling_game(TestCase):

    def setUp(self):
        self.game = BowlingGame()

    def roll_alot(self, rolls, pins):
        for roll in range(rolls):
            self.game.roll(pins)

    def test_gutter_game(self):
        self.roll_alot(20, 0)
        self.assertEqual(self.game.score(), 0)

    def test_all_ones_game(self):
        self.roll_alot(20, 1)
        self.assertEqual(self.game.score(), 20)

    def test_one_spare(self):
        self.game.roll(7)
        self.game.roll(3) # a spare
        self.roll_alot(18, 1)
        self.assertEqual(self.game.score(), 29)
        
    def test_one_strike(self):
        self.roll_alot(10, 0)
        self.game.roll(10) # a strike
        self.roll_alot(8, 1)
        self.assertEqual(self.game.score(), 20)


