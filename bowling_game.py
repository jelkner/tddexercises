class BowlingGame:

    def __init__(self):
        self.rolls = []

    def roll(self, pins):
        if not self.rolls or len(self.rolls[-1]) == 2:
            if pins == 10:
                self.rolls.append([pins, 0]) 
            else:
                self.rolls.append([pins]) 
        else:
            self.rolls[-1].append(pins)

    def score(self):
        total_score = 0

        for frame_num in range(len(self.rolls)):
            if self.rolls[frame_num][0] == 10:
                total_score += 10 + sum(self.rolls[frame_num + 1])
            else:
                frame_score = sum(self.rolls[frame_num])
                if frame_score == 10:
                    total_score += frame_score + self.rolls[frame_num + 1][0]
                else:
                    total_score += frame_score

        return total_score

        
    
