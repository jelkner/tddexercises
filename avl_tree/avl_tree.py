class AVL:
    """
    Implement an AVL tree
    """

    def __init__(self, key_or_items, value=None, height=0):
        if isinstance(key_or_items, dict):
            items = list(key_or_items.items())
            self.key = items[0][0]
            self.value = items[0][1]
            self.height = height
            self.left = AVL(*items[2], height=self.height + 1)
            self.right = AVL(*items[1], height=self.height + 1)

        else:
            self.key = key_or_items
            self.value = value
            self.height = height
            self.left = None
            self.right = None

    def recalculate_height(self):
        if self.left is None and self.right is None:
            self.height = 0
        elif self.left is None or self.right is None:
            self.height = 1
        else:
            self.left.recalculate_height()
            self.right.recalculate_height()
            self.height = 1 + max(self.left.height, self.right.height)
